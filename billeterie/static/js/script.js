var bar = document.getElementById("search");
bar.onclick = function()
{
    if (this.classList.contains("placeholder"))
    {
        this.classList.remove("placeholder");
        this.value = "";
    }
};
bar.onblur = function()
{
    if (this.value == "")
    {
        this.classList.add("placeholder");
        this.value = "Rechercher...";
    }
};

var body = document.getElementsByClassName("body")[0];
var header = document.getElementsByClassName("header")[0];
var navigation = document.getElementsByClassName("navigation")[0];
var height = window.innerHeight - header.offsetHeight - navigation.offsetHeight - 30;
body.style.minHeight = height + "px";