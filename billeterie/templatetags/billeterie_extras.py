from django import template

from billeterie.models import *

register = template.Library()
try:
    group = Group.objects.get(name="Responsable associations")
except:
    group = None


@register.filter
def slice_around(value, arg):
    return value[max(arg - 3, 0), arg + 3]


# You can add more templatetags here
@register.filter(name='has_group')
def has_group(user, group_name):
    group = Group.objects.get(name=group_name)
    return True if group in user.groups.all() else False


@register.filter(name='has_perm')
def has_perm(user, perm_name):
    return True if perm_name in user.get_all_permissions() else False


@register.filter(name='has_perm_chef')
def has_perm_chef(user, asso_id):
    if not user.is_authenticated:
        return False
    else:
        return user.is_superuser or group in user.groups.all() \
            or Association.objects.get(name=asso_id).chef.user.username == user.username


@register.filter(name='has_perm_creator_event')
def has_perm_creator_event(user, event_id):
    if not user.is_authenticated:
        return False
    else:
        event = Event.objects.get(id=event_id)
        return user.is_superuser or group in user.groups.all() \
            or event.association.chef.user.username == user.username \
            or event.creator.user.username == user.username


@register.filter(name='has_perm_staff_event')
def has_perm_staff_event(user, event_id):
    if not user.is_authenticated:
        return False
    elif user.is_superuser or group in user.groups.all():
        return True
    else:
        person = Person.objects.get(user=user)
        event = Event.objects.get(id=event_id)
        staff = EventStaff.objects.get(event=event, user=person)
        return user.is_superuser or group in user.groups.all() \
            or staff.user.user.username == user.username


@register.filter(name='has_perm_member_asso')
def has_perm_member_asso(user):
    if not user.is_authenticated:
        return False
    elif user.is_superuser or group in user.groups.all():
        return True
    else:
        for asso in Association.objects.all():
            if asso.chef.user.username == user.username:
                return True
        for assomember in AssociationMember.objects.filter(user=Person.objects.get(user=user)):
            if assomember.user.user.username == user.username:
                return True
        return False
