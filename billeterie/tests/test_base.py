from django.test import TestCase
import os
from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'yakastar.settings')
application = get_wsgi_application()


class BasicTest(TestCase):

    fixtures = ('fixtures.yaml', )

    def test_index_exists(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_epita_exists(self):
        response = self.client.get('/epita/')
        self.assertEqual(response.status_code, 302)

    def test_login_exists(self):
        response = self.client.get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_process_payment_exists(self):
        response = self.client.get('/process_payment/')
        self.assertEqual(response.status_code, 302)

    def test_persons_exists(self):
        response = self.client.get('/persons/')
        self.assertEqual(response.status_code, 200)

    def test_no_places_remaining_exists(self):
        response = self.client.get('/no_places_remaining/')
        self.assertEqual(response.status_code, 200)

    def test_search_list_exists(self):
        response = self.client.get('/search_list/')
        self.assertEqual(response.status_code, 200)

    def test_already_register_exists(self):
        response = self.client.get('/already_register/')
        self.assertEqual(response.status_code, 200)

    def test_register_exists(self):
        response = self.client.get('/register/')
        self.assertEqual(response.status_code, 200)

    def test_registration_exists(self):
        response = self.client.get('/registration/')
        self.assertEqual(response.status_code, 200)

    def test_admin_exists(self):
        response = self.client.get('/admin/')
        self.assertEqual(response.status_code, 302)

    def test_404_exists(self):
        response = self.client.get('/unknown/')
        self.assertEqual(response.status_code, 404)
