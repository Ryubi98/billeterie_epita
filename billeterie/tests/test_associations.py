from django.test import TestCase
import os
from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'yakastar.settings')
application = get_wsgi_application()

from billeterie.models import *


class BasicTest(TestCase):

    fixtures = ('fixtures.yaml', )

    def test_associations_exists(self):
        response = self.client.get('/associations/')
        self.assertEqual(response.status_code, 200)

    def test_association_exists(self):
        association = Association.objects.all()[0]
        response = self.client.get('/associations/' + str(association.id))
        self.assertEqual(response.status_code, 200)

    def test_association_already_exists(self):
        response = self.client.get('/association_exist/')
        self.assertEqual(response.status_code, 200)
