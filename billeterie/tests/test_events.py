from django.test import TestCase
import os
from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'yakastar.settings')
application = get_wsgi_application()

from billeterie.models import *


class BasicTest(TestCase):

    fixtures = ('fixtures.yaml', )

    def test_events_exists(self):
        response = self.client.get('/events/')
        self.assertEqual(response.status_code, 200)

    def test_event_exists(self):
        event = Event.objects.all()[0]
        response = self.client.get('/events/' + str(event.id))
        self.assertEqual(response.status_code, 200)

    def test_create_event_exists(self):
        response = self.client.get('/create_event/')
        self.assertEqual(response.status_code, 302)
