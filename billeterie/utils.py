from calendar import HTMLCalendar
from .models import *


class Calendar(HTMLCalendar):
    def __init__(self, year=None, month=None):
        self.year = year
        self.month = month
        super(Calendar, self).__init__()

    # formats a day as a td
    # filter events by day
    def formatday(self, day, events, person):
        events_per_day = events.filter(begin_date__day=day)
        d = ''
        for event in events_per_day:
            if person is None:
                d += f'<li><a href="/events/' + str(event.id) + '">' + event.title + '</a></li>'
            elif EventStaff.objects.filter(user=person.id, event=event.id).exists():
                d += f'<li><a  style="color: red" href="/events/' + str(event.id) + '">' + event.title + '</a></li>'
            elif EventMember.objects.filter(user=person.id, event=event.id).exists():
                d += f'<li><a  style="color: green" href="/events/' + str(event.id) + '">' + event.title + '</a></li>'
            elif person.user.username == Event.objects.get(id=event.id).creator.user.username:
                d += f'<li><a  style="color: maroon" href="/events/' + str(event.id) + '">' + event.title + '</a></li>'
            elif person.user.username == Event.objects.get(id=event.id).association.chef.user.username:
                d += f'<li><a  style="color: purple" href="/events/' + str(event.id) + '">' + event.title + '</a></li>'
            else:
                d += f'<li><a href="/events/' + str(event.id) + '">' + event.title + '</a></li>'

        if day != 0:
            return f"<td><span class='date'>{day}</span><ul> {d} </ul></td>"
        return '<td></td>'

    # formats a week as a tr
    def formatweek(self, theweek, events, person):
        week = ''
        for d, weekday in theweek:
            week += self.formatday(d, events, person)
        return f'<tr> {week} </tr>'

    # formats a month as a table
    # filter events by year and month
    def formatmonth(self, person, withyear=True):
        events = Event.objects.filter(begin_date__year=self.year, begin_date__month=self.month)

        cal = f'<table border="0" cellpadding="0" cellspacing="0" class="calendar">\n'
        cal += f'{self.formatmonthname(self.year, self.month, withyear=withyear)}\n'
        cal += f'{self.formatweekheader()}\n'
        for week in self.monthdays2calendar(self.year, self.month):
            cal += f'{self.formatweek(week, events, person)}\n'
        return cal
