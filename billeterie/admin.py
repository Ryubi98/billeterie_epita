from django.contrib import admin
from .models import *

# Register your models here.

admin.site.register(Event)
admin.site.register(Person)
admin.site.register(Association)
admin.site.register(AssociationMember)
admin.site.register(EventMember)
admin.site.register(EventStaff)
