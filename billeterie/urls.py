from django.urls import path

from billeterie import views
from billeterie.views import *

app_name = "billeterie"
urlpatterns = [

    path('', views.IndexView.as_view(), name='index'),

    path('no_places_remaining/', views.NoPlacesView.as_view(), name='no_places_remaining'),
    path('search_list/', views.SearchListView.as_view(), name='search_list'),
    path('already_register/', views.AlreadyRegisterView.as_view(), name='already_register'),
    path('register/', views.RegisterView.as_view(), name='register'),

    path('persons/', views.PersonsView.as_view(), name='persons'),

    path('events/', views.EventsView.as_view(), name='events'),
    path('create_event/', views.upload_event, name='create_event'),
    path('events/<int:event_id>', views.event, name='event'),
    path('events/<int:event_id>/delete', views.event_deleted, name='event_deleted'),
    path('events/<int:event_id>/modified', views.event_modified, name='event_modified'),
    path('events/<int:event_id>/add_member', views.event_add_member, name='event_add_member'),
    path('events/<int:event_id>/add_staff', views.event_add_staff, name='event_add_staff'),
    path('events/<int:event_id>/delete_staff', views.event_delete_staff, name='event_delete_staff'),
    path('events/<int:event_id>/validate', views.event_validate, name='event_validate'),
    path('events/<int:event_id>/refuse', views.event_refuse, name='event_refuse'),

    path('associations/', views.AssociationsView.as_view(), name='associations'),
    path('association_exist/', views.AssociationExistView.as_view(), name='association_exist'),
    path('create_association/', views.upload_association, name='create_association'),
    path('associations/<int:association_id>', views.association, name='association'),
    path('associations/<int:association_id>/delete', views.association_deleted, name='association_deleted'),
    path('associations/<int:association_id>/modified', views.association_modified, name='association_modified'),
    path('associations/<int:association_id>/add_member', views.association_add_member, name='association_add_member'),
    path('associations/<int:association_id>/delete_member', views.association_delete_member, name='association_delete_member'),

    path('profile/<int:user_id>/modified', views.profile_modified, name='profile_modified'),
    path('profile/<str:validation_code>', views.profile_billet, name='profile_billet'),
    path('profile/<int:event_id>/search', views.search_billet, name='search_billet'),
    path('profile/<int:event_id>/list', views.list_billet, name='list_billet'),

    ###########
    path('login/', views.login, name='login'),
    path('logged/', views.logged, name='logged'),
    path('profile/', views.profile, name='profile'),
    path('registration/', views.registration, name='registration'),
    path('accounts/birthdate', views.person, name='birthdate'),

    path('client-autocomplete/', ClientAutocomplete.as_view(), name='client-autocomplete'),

    path('epita/', views.epita, name="epita"),

    path('process_payment/', views.process_payment, name="process_payment"),
    path('payment_done/', views.payment_done, name="payment_done"),
    path('payment_canceled', views.payment_canceled, name="payment_canceled"),
]
