import calendar
import datetime
import random
import string
from datetime import datetime as dt

from django.forms import ModelForm
from django.shortcuts import redirect, render, get_object_or_404, reverse
from django.conf import settings
from django.utils.safestring import mark_safe
from django.views import generic
from django.views.decorators.csrf import csrf_exempt
from django.core.mail import send_mail

from paypal.standard.forms import PayPalPaymentsForm
from dal import autocomplete
from billeterie.utils import Calendar
from billeterie.templatetags.billeterie_extras import *


# Create your views here.
class IndexView(generic.ListView):
    model = Event
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # use today's date for the calendar
        d = get_date(self.request.GET.get('month', None))
        context['prev_month'] = prev_month(d)
        context['next_month'] = next_month(d)

        # Instantiate our calendar class with today's year and date
        cal = Calendar(d.year, d.month)

        # Call the formatmonth method, which returns our calendar as a table
        person = None
        if self.request.user.is_authenticated:
            person = Person.objects.get(user=self.request.user)
        html_cal = cal.formatmonth(person)
        context['calendar'] = mark_safe(html_cal)
        context['events'] = Event.objects.filter(info_type='CLOSED') or Event.objects.filter(info_type='VALIDATED')
        return context


def get_date(req_day):
    if req_day:
        year, month = (int(x) for x in req_day.split('-'))
        return datetime.date(year, month, day=1)
    return dt.today()


def prev_month(d):
    first = d.replace(day=1)
    prev_month = first - datetime.timedelta(days=1)
    month = 'month=' + str(prev_month.year) + '-' + str(prev_month.month)
    return month


def next_month(d):
    days_in_month = calendar.monthrange(d.year, d.month)[1]
    last = d.replace(day=days_in_month)
    next_month = last + datetime.timedelta(days=1)
    month = 'month=' + str(next_month.year) + '-' + str(next_month.month)
    return month


class NoPlacesView(generic.ListView):
    template_name = 'no_places_remaining.html'

    def get_queryset(self):
        return []


class AlreadyRegisterView(generic.ListView):
    template_name = 'already_register.html'

    def get_queryset(self):
        return []


class RegisterView(generic.ListView):
    template_name = 'register.html'

    def get_queryset(self):
        return []


class EventsView(generic.ListView):
    model = Event
    template_name = 'events.html'
    paginate_by = 10

    def get_queryset(self):
        events = Event.objects.all()
        for event in events:
            if event.end_date <= timezone.now():
                Event.objects.filter(id=event.id).update(info_type='TERMINATED')
            elif event.end_inscription <= timezone.now():
                Event.objects.filter(id=event.id).update(info_type='CLOSED')
        qs = self.model._default_manager.get_queryset()
        qs = qs.annotate(custom_order=
        models.Case(
            models.When(info_type='VALIDATED', then=models.Value(0)),
            models.When(info_type='CLOSED', then=models.Value(1)),
            models.When(info_type='WAITING', then=models.Value(2)),
            models.When(info_type='TERMINATED', then=models.Value(3)),
            models.When(info_type='REFUSED', then=models.Value(4)),
            default=models.Value(5),
            output_field=models.IntegerField(), )
        ).order_by('custom_order')
        return qs


class PersonsView(generic.ListView):
    model = Person
    template_name = 'persons.html'
    paginate_by = 10

    def get_queryset(self):
        return Person.objects.order_by('user')


class AssociationsView(generic.ListView):
    model = Association
    template_name = 'associations.html'
    paginate_by = 10

    def get_queryset(self):
        return Association.objects.order_by('name')


class AssociationExistView(generic.ListView):
    template_name = 'association_exist.html'

    def get_queryset(self):
        return []


class SearchListView(generic.ListView):
    """
    Display a List page filtered by the search query.
    """
    template_name = 'search.html'
    paginate_by = 10

    def get_queryset(self):
        return []

    def get_context_data(self, **kwargs):
        context = super(SearchListView, self).get_context_data(**kwargs)
        qa = Association.objects.all()
        qe = Event.objects.all()
        keywords = self.request.GET.get('q')
        if keywords:
            qa = Association.objects.filter(name__icontains=keywords)
            qe = Event.objects.filter(title__icontains=keywords)
        context['associations'] = qa
        context['events'] = qe
        return context


class ClientAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        # if not self.request.user.is_authenticated():
        #    return User.objects.none()

        qs = Person.objects.all()

        if self.q:
            qs = qs.filter(user__username__icontains=self.q)

        return qs


class CreateEventForm(ModelForm):
    association = forms.ModelChoiceField(queryset=Association.objects.all())

    class Meta:
        model = Event
        fields = ['title', 'description', 'begin_date',
                  'end_date', 'end_inscription', 'place',
                  'image', 'association', 'max_intern', 'max_extern',
                  'price_intern', 'price_extern',
                  'display_remaining_places']

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(CreateEventForm, self).__init__(*args, **kwargs)
        assos = AssociationMember.objects.filter(user=Person.objects.get(user=user))
        self.fields['association'].queryset = Association.objects.filter(associationmember__in=assos) or Association.objects.filter(chef=Person.objects.get(user=user))


class CreateAssociationForm(ModelForm):
    chef = forms.ModelChoiceField(
        queryset=Person.objects.all(),
        widget=autocomplete.ModelSelect2(url='/client-autocomplete')
    )

    class Meta:
        model = Association
        fields = ['name', 'chef']


class AddAssociationMemberForm(ModelForm):
    user = forms.ModelChoiceField(
        queryset=Person.objects.all(),
        widget=autocomplete.ModelSelect2(url='/client-autocomplete')
    )

    class Meta:
        model = AssociationMember
        fields = ['user', 'grade']


class DeleteAssociationMemberForm(ModelForm):
    user = forms.ModelChoiceField(
        queryset=Person.objects.all(),
    )

    class Meta:
        model = AssociationMember
        fields = ['user']

    def __init__(self, *args, **kwargs):
        association_id = kwargs.pop('association_id', None)
        super(DeleteAssociationMemberForm, self).__init__(*args, **kwargs)
        association = Association.objects.get(id=association_id)
        members = AssociationMember.objects.filter(association=association)
        self.fields['user'].queryset = Person.objects.filter(associationmember__in=members)


class DeleteEventStaffForm(ModelForm):
    user = forms.ModelChoiceField(
        queryset=Person.objects.all(),
    )

    class Meta:
        model = EventStaff
        fields = ['user']

    def __init__(self, *args, **kwargs):
        association_id = kwargs.pop('association_id', None)
        super(DeleteEventStaffForm, self).__init__(*args, **kwargs)
        association = Association.objects.get(id=association_id)
        members = AssociationMember.objects.filter(association=association)
        self.fields['user'].queryset = Person.objects.filter(associationmember__in=members)


class AddEventStaffForm(ModelForm):
    user = forms.ModelChoiceField(
        queryset=Person.objects.all(),
    )

    class Meta:
        model = EventStaff
        fields = ['user']

    def __init__(self, *args, **kwargs):
        association_id = kwargs.pop('association_id', None)
        super(AddEventStaffForm, self).__init__(*args, **kwargs)
        association = Association.objects.get(id=association_id)
        members = AssociationMember.objects.filter(association=association)
        self.fields['user'].queryset = Person.objects.filter(associationmember__in=members)


class AddEventMemberForm(ModelForm):
    class Meta:
        model = EventMember
        fields = []


class DOBRegistrationForm(RegistrationForm):
    date_of_birth = forms.DateField(widget=forms.SelectDateWidget(years=range(1900, timezone.now().year + 1)),
                                    initial=timezone.now)

    def clean_date_of_birth(self):
        dob = self.cleaned_data['date_of_birth']
        return dob


def epita(request):
    return redirect('http://www.epita.fr')


def send_mail_creation_event(form):
    association = form.cleaned_data.get('association')
    mail_chef_asso = association.chef.user.email
    msg = "Un événement à été créer et a besoin de votre attention.\nCet événement est "
    msg += form.cleaned_data.get('title')
    msg += ".\nVeuillez le valider ou le refuser."
    send_mail("Création d'événement",
              msg,
              'noreply.billeterie.pepita@gmail.com',
              [mail_chef_asso],
              fail_silently=False)


def send_mail_modify_event(form, event_id):
    event = Event.objects.get(id=event_id)
    mail_chef_asso = event.association.chef.user.email
    mail_creator_event = event.creator.user.email
    mail_members = []
    for member in EventMember.objects.filter(event=event):
        mail_members.append(member.user.user.email)
    msg = "Un événement à été modifier et a besoin de votre attention.\nCet événement est "
    msg += form.cleaned_data['title']
    msg += ".\nVeuillez regarder les modifications et le valider à nouveau ou le refuser."
    send_mail("Modification d'événement",
              msg,
              'noreply.billeterie.pepita@gmail.com',
              [mail_chef_asso],
              fail_silently=False)
    msg = "Un événement auquel vous participer à été modifier.\n"
    msg += "Veuillez le consultez sur le site pour voir les modifications de l'événement "
    msg += form.cleaned_data['title']
    send_mail("Modification d'événement",
              msg,
              'noreply.billeterie.pepita@gmail.com',
              mail_members,
              fail_silently=False)
    msg = "Un événement que vous avez crééer à été modifier.\n"
    msg += "Veuillez le consultez sur le site pour voir les modifications de l'événement "
    msg += form.cleaned_data['title']
    send_mail("Modification d'événement",
              msg,
              'noreply.billeterie.pepita@gmail.com',
              [mail_creator_event],
              fail_silently=False)


def send_mail_delete_event(event_id):
    event = Event.objects.get(id=event_id)
    mail_chef_asso = event.association.chef.user.email
    mail_creator_event = event.creator.user.email
    mail_members = []
    for member in EventMember.objects.filter(event=event):
        mail_members.append(member.user.user.email)
    msg = "L'événement "
    msg += event.title
    msg += "de votre association à été supprimé.\n"
    send_mail("Suppression d'événement",
              msg,
              'noreply.billeterie.pepita@gmail.com',
              [mail_chef_asso],
              fail_silently=False)
    msg = "L'événement "
    msg += event.title
    msg += " auquel vous participer à été supprimé.\n"
    send_mail("Suppression d'événement",
              msg,
              'noreply.billeterie.pepita@gmail.com',
              mail_members,
              fail_silently=False)
    msg = "L'événement "
    msg += event.title
    msg += " que vous créé à été supprimé.\n"
    send_mail("Suppression d'événement",
              msg,
              'noreply.billeterie.pepita@gmail.com',
              [mail_creator_event],
              fail_silently=False)


def send_mail_add_member_event(event_id, user, vd):
    event = Event.objects.get(id=event_id)
    member_mail = Person.objects.get(user=user.user).user.email
    msg = "Vous êtes inscrits à l'événement "
    msg += event.title
    msg += ".\n Votre code de validation est "
    msg += vd
    msg += ".\nPour voir votre billet allez dans la rubrique Mon Compte sur le site web.\n"
    msg += "N'oubliez pas votre pièce d'identité lors de l'événement.\n"
    send_mail("Inscription à un événement",
              msg,
              'noreply.billeterie.pepita@gmail.com',
              [member_mail],
              fail_silently=False)


def send_mail_add_staff_event(event_id, user):
    event = Event.objects.get(id=event_id)
    staff_mail = Person.objects.get(user=user.user).user.email
    msg = "Vous avez été ajouté à l'événement "
    msg += event.title
    msg += " en tant que STAFF.\n"
    send_mail("Ajout staff d'un événement",
              msg,
              'noreply.billeterie.pepita@gmail.com',
              [staff_mail],
              fail_silently=False)


def send_mail_delete_staff_event(event_id, user):
    event = Event.objects.get(id=event_id)
    staff_mail = Person.objects.get(user=user.user).user.email
    msg = "Vous avez été retiré à l'événement "
    msg += event.title
    msg += " en tant que STAFF.\n"
    send_mail("Supression staff d'un événement",
              msg,
              'noreply.billeterie.pepita@gmail.com',
              [staff_mail],
              fail_silently=False)


def send_mail_validate_event(event_id):
    event = Event.objects.get(id=event_id)
    mail_creator_event = event.creator.user.email
    msg = "L'événement "
    msg += event.title
    msg += " que vous créé à été validé.\n"
    send_mail("Validation d'événement",
              msg,
              'noreply.billeterie.pepita@gmail.com',
              [mail_creator_event],
              fail_silently=False)


def send_mail_refuse_event(event_id):
    event = Event.objects.get(id=event_id)
    mail_creator_event = event.creator.user.email
    msg = "L'événement "
    msg += event.title
    msg += " que vous créé à été refusé.\n"
    send_mail("Validation d'événement",
              msg,
              'noreply.billeterie.pepita@gmail.com',
              [mail_creator_event],
              fail_silently=False)


def event(request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    rpi = event.max_intern - EventMember.objects.filter(user__in=Person.objects.filter(is_intern=True),
                                                        event=event).count()
    rpe = event.max_extern - EventMember.objects.filter(user__in=Person.objects.filter(is_intern=False),
                                                        event=event).count()
    return render(request, 'event.html', {
        'event': event,
        'rpi': rpi,
        'rpe': rpe
    })


def upload_event(request):
    if not has_perm_member_asso(request.user):
        return redirect('/')
    if request.method == 'POST':
        form = CreateEventForm(request.POST, request.FILES, user=request.user)
        if form.is_valid():
            Event.objects.create(
                title=form.cleaned_data.get('title'),
                description=form.cleaned_data.get('description'),
                begin_date=form.cleaned_data.get('begin_date'),
                end_date=form.cleaned_data.get('end_date'),
                end_inscription=form.cleaned_data.get('end_inscription'),
                place=form.cleaned_data.get('place'),
                image=form.cleaned_data.get('image'),
                association=form.cleaned_data.get('association'),
                max_intern=form.cleaned_data.get('max_intern'),
                max_extern=form.cleaned_data.get('max_extern'),
                price_intern=form.cleaned_data.get('price_intern'),
                price_extern=form.cleaned_data.get('price_extern'),
                display_remaining_places=form.cleaned_data.get('display_remaining_places'),
                info_type='WAITING',
                creator=Person.objects.get(user=request.user)
            )
            send_mail_creation_event(form)
            return redirect('/events')
    else:
        form = CreateEventForm(user=request.user)
    return render(request, 'create_event.html', {
        'form': form
    })


def creator_modify_event(request, event, form):
    if request.user.is_superuser or group in request.user.groups.all() or \
            request.user.username == event.association.chef.user.username:
        return False
    else:
        return event.creator.user.username == request.user.username \
               and (form.cleaned_data['title'] != event.title
                    or form.cleaned_data['place'] != event.place
                    or form.cleaned_data['price_extern'] != event.price_extern
                    or form.cleaned_data['price_intern'] != event.price_intern
                    or form.cleaned_data['max_extern'] != event.max_extern
                    or form.cleaned_data['max_intern'] != event.max_intern
                    or form.cleaned_data['begin_date'] != event.begin_date
                    or form.cleaned_data['end_date'] != event.end_date
                    or form.cleaned_data['end_inscription'] != event.end_inscription)


def event_modified(request, event_id):
    if not has_perm_creator_event(request.user, event_id):
        return redirect('/')
    event = get_object_or_404(Event, pk=event_id)
    if request.method == 'POST':
        save = Event.objects.get(id=event.id)
        save.pk = None
        form = CreateEventForm(request.POST, instance=event, user=request.user)
        if form.is_valid():
            if creator_modify_event(request, save, form):
                event.info_type = 'WAITING'
            elif form.cleaned_data['end_inscription'] != event.end_inscription:
                if form.cleaned_data['end_inscription'] > timezone.now():
                    Event.objects.filter(id=event.id).update(info_type='VALIDATED')
            send_mail_modify_event(form, event_id)
            form.save()
            return redirect('/events')
    else:
        form = CreateEventForm(instance=event, user=request.user)
    return render(request, 'modify_event.html', {
        'form': form
    })


def event_deleted(request, event_id):
    if not has_perm_creator_event(request.user, event_id):
        return redirect('/')
    event = get_object_or_404(Event, pk=event_id)
    send_mail_delete_event(event_id)
    event.delete()
    return redirect('/events')


def event_add_member(request, event_id):
    if not request.user.is_authenticated:
        return redirect('/accounts/login')
    event = get_object_or_404(Event, pk=event_id)
    rpi = event.max_intern - EventMember.objects.filter(user__in=Person.objects.filter(is_intern=True),
                                                        id=event_id).count()
    rpe = event.max_extern - EventMember.objects.filter(user__in=Person.objects.filter(is_intern=False),
                                                        id=event_id).count()
    person = Person.objects.get(user=request.user)
    if person.is_intern:
        amount = event.price_intern
    else:
        amount = event.price_extern

    if (person.is_intern and rpi == 0) or (not person.is_intern and rpe == 0):
        return redirect('/no_places_remaining')
    if EventMember.objects.filter(user=person, event=event).exists():
        return redirect('/already_register')
    if amount == 0:
        vds = EventMember.objects.all().values_list('validation_code', flat=True)
        vd = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(6))
        while vd in vds:
            vd = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(6))
        EventMember.objects.create(user=person, event=event, validation_code=vd)
        send_mail_add_member_event(event_id, person, vd)
        return redirect('/register')
    if request.method == 'POST':
        request.session['event_id'] = event_id
        return redirect('/process_payment')
    return render(request, 'event_add_member.html', {
        'event_id': event_id
    })


def event_add_staff(request, event_id):
    if not has_perm_creator_event(request.user, event_id):
        return redirect('/')
    event = get_object_or_404(Event, pk=event_id)
    if request.method == 'POST':
        form = AddEventStaffForm(request.POST, request.FILES, association_id=event.association.id)
        if form.is_valid():
            if not EventStaff.objects.filter(user=form.cleaned_data.get("user"), event=event).exists():
                user = form.cleaned_data.get("user")
                EventStaff.objects.create(user=user, event=event)
                send_mail_add_staff_event(event_id, user)
            return redirect('/events/' + str(event_id))
    else:
        form = AddEventStaffForm(association_id=event.association.id)
    return render(request, 'event_add_staff.html', {
        'form': form,
        'event_id': event_id
    })


def event_delete_staff(request, event_id):
    if not has_perm_creator_event(request.user, event_id):
        return redirect('/')
    event = get_object_or_404(Event, pk=event_id)
    if request.method == 'POST':
        form = DeleteEventStaffForm(request.POST, request.FILES, association_id=event.association.id)
        if form.is_valid():
            user = form.cleaned_data.get("user")
            EventStaff.objects.filter(user=user, event=event).delete()
            send_mail_delete_staff_event(event_id, user)
            return redirect('/events/' + str(event_id))
    else:
        form = DeleteEventStaffForm(association_id=event.association.id)
    return render(request, 'event_add_staff.html', {
        'form': form,
        'event_id': event_id
    })


def event_validate(request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    if not has_perm_chef(request.user, event.association):
        return redirect('/')
    Event.objects.filter(id=event_id).update(info_type='VALIDATED')
    send_mail_validate_event(event_id)
    return redirect("/events")


def event_refuse(request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    if not has_perm_chef(request.user, event.association):
        return redirect('/')
    Event.objects.filter(id=event_id).update(info_type='REFUSED')
    send_mail_refuse_event(event_id)
    return redirect("/events")


def association(request, association_id):
    association = get_object_or_404(Association, pk=association_id)
    return render(request, 'association.html', {
        'association': association,
        'members': AssociationMember.objects.filter(association=association),
        'events': Event.objects.filter(association=association)
    })


def upload_association(request):
    if not has_perm(request.user, 'billeterie.add_association'):
        return redirect('/')
    if request.method == 'POST':
        form = CreateAssociationForm(request.POST, request.FILES)
        if form.is_valid():
            if Association.objects.filter(name=form.cleaned_data['name']).exists():
                return redirect('/association_exist')
            form.save()
            return redirect('/associations')
    else:
        form = CreateAssociationForm()
    return render(request, 'create_association.html', {
        'form': form
    })


def association_modified(request, association_id):
    if not has_perm(request.user, 'billeterie.add_association'):
        return redirect('/')
    association = get_object_or_404(Association, pk=association_id)
    if request.method == 'POST':
        form = CreateAssociationForm(request.POST, instance=association)
        if form.is_valid():
            form.save()
            return redirect('/associations')
    else:
        form = CreateAssociationForm(instance=association)
    return render(request, 'modify_association.html', {
        'form': form
    })


def association_deleted(request, association_id):
    if not has_perm(request.user, 'billeterie.add_association'):
        return redirect('/')
    association = get_object_or_404(Association, pk=association_id)
    user = User.objects.get(username=association.chef.user.username)
    association.delete()
    return redirect('/associations')


def association_add_member(request, association_id):
    association = get_object_or_404(Association, pk=association_id)
    if not has_perm_chef(request.user, association.name):
        return redirect('/')
    if request.method == 'POST':
        form = AddAssociationMemberForm(request.POST, request.FILES)
        if form.is_valid():
            if not AssociationMember.objects.filter(user=form.cleaned_data['user'], association=association).exists():
                user = form.cleaned_data.get("user")
                grade = form.cleaned_data.get("grade")
                AssociationMember.objects.create(user=user, association=association, grade=grade)
                user = User.objects.get(username=user)
        return redirect('/associations/' + str(association_id))
    else:
        form = AddAssociationMemberForm()
    return render(request, 'association_add_member.html', {
        'form': form,
        'association_id': association_id
    })


def association_delete_member(request, association_id):
    association = get_object_or_404(Association, pk=association_id)
    if not has_perm_chef(request.user, association.name):
        return redirect('/')
    if request.method == 'POST':
        form = DeleteAssociationMemberForm(request.POST, request.FILES, association_id=association.id)
        if form.is_valid():
            user = form.cleaned_data.get("user")
            AssociationMember.objects.filter(user=user, association=association).delete()
        return redirect('/associations/' + str(association_id))
    else:
        form = DeleteAssociationMemberForm(association_id=association.id)
    return render(request, 'association_delete_member.html', {
        'form': form,
        'association_id': association_id
    })


def profile_modified(request, user_id):
    if not request.user.is_authenticated:
        return redirect('/login')
    user = get_object_or_404(User, pk=user_id)
    if request.user.username == user.username:
        date = Person.objects.get(user=user).birth_date
        if request.method == 'POST':
            form = DOBRegistrationForm(request.POST, initial={'date_of_birth': date}, instance=user)
            if form.is_valid():
                form.save()
                user = User.objects.get(username=form.cleaned_data.get("username"))
                Person.objects.filter(user=user).update(birth_date=form.cleaned_data.get("date_of_birth"))
                return redirect('/profile')
        else:
            form = DOBRegistrationForm(initial={'date_of_birth': date}, instance=user)
        return render(request, 'modify_profile.html', {
            'form': form
        })
    else:
        return redirect('/login')


def login(request):
    if request.user.is_authenticated:
        return redirect('/profile')
    return render(request, 'login.html')


def profile(request):
    if request.user.is_authenticated:
        context = {
            'user': request.user,
            'person': Person.objects.get(user=request.user),
            'events': EventMember.objects.filter(user=Person.objects.get(user=request.user)),
            'associations': AssociationMember.objects.filter(user=Person.objects.get(user=request.user)),
            'staffs': EventStaff.objects.filter(user=Person.objects.get(user=request.user)),
            'eventschef': Event.objects.filter(association__in=Association.objects.filter(chef=Person.objects.get(user=request.user))),
            'chefs': Association.objects.filter(chef=Person.objects.get(user=request.user))
        }
        return render(request, 'profile.html', context=context)
    else:
        return redirect('/login')


def profile_billet(request, validation_code):
    if not request.user.is_authenticated:
        return redirect('/login')
    event_member = get_object_or_404(EventMember, validation_code=validation_code)
    person = Person.objects.get(user=request.user)
    event = event_member.event
    event_date = event.begin_date.date()
    event_hour = event.begin_date.time()
    return render(request, 'billet.html', {
        'event_member': event_member,
        'person': person,
        'event': event,
        'event_date': event_date,
        'event_hour': event_hour,
    })


def search_billet(request, event_id):
    if not has_perm_staff_event(request.user, event_id):
        return redirect('/login')
    member = None
    event = Event.objects.get(id=event_id)
    name = event.title
    if request.GET.get('search'):
        search = request.GET.get('search')
        member = EventMember.objects.get(validation_code__contains=search, event=event)

    return render(request, 'billet_search.html', {
        'member': member,
        'name': name
    })


def list_billet(request, event_id):
    if not has_perm_staff_event(request.user, event_id):
        return redirect('/login')
    event = Event.objects.get(id=event_id)
    members = EventMember.objects.filter(event=event)
    name = event.title
    return render(request, 'billet_list.html', {
        'members': members,
        'name': name
    })


def logged(request):
    if request.user.is_authenticated:
        user = User.objects.get(username=request.user.username)
        if not Person.objects.filter(user=user):
            return redirect('/accounts/birthdate')
        return redirect('/')
    else:
        return redirect('/login')


def person(request):
    if request.method == 'POST':
        form = DateForm(request.POST)
        if form.is_valid():
            user = User.objects.get(username=request.user.username)
            Person.objects.create(user=user, birth_date=form.cleaned_data.get("birth_date"), is_intern=True)
            return redirect('/')
    else:
        form = DateForm()
    return render(request, 'birthdate.html', {
        'form': form
    })


def registration(request):
    if request.method == 'POST':
        form = DOBRegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            user = User.objects.get(username=form.cleaned_data.get("username"))
            Person.objects.create(user=user, birth_date=form.cleaned_data.get("date_of_birth"), is_intern=False)
            return redirect('/accounts/login')
    else:
        form = DOBRegistrationForm()
    return render(request, 'registration/registration.html', {'form': form})


def process_payment(request):
    if not request.user.is_authenticated:
        return redirect('/login')
    event_id = request.session.get('event_id')
    current_event = get_object_or_404(Event, id=event_id)
    person = Person.objects.get(user=request.user)
    if person.is_intern:
        amount = current_event.price_intern
    else:
        amount = current_event.price_extern

    host = request.get_host()

    paypal_dict = {
        'business': settings.PAYPAL_RECEIVER_EMAIL,
        'amount': amount,
        'item_name': current_event.title,
        'invoice': str(current_event.id),
        'currency_code': 'EUR',
        'notify_url': 'http://{}{}'.format(host, reverse('paypal-ipn')),
        'return_url': 'http://{}{}'.format(host, reverse('billeterie:payment_done')),
        'cancel_return': 'http://{}{}'.format(host, reverse('billeterie:payment_canceled')),
    }

    form = PayPalPaymentsForm(initial=paypal_dict)
    request.session['event_id'] = event_id
    return render(request, 'make_payment.html', {'order': current_event, 'form': form})


@csrf_exempt
def payment_done(request):
    event_id = request.session.get('event_id')
    event = get_object_or_404(Event, id=event_id)
    person = Person.objects.get(user=request.user)
    vds = EventMember.objects.all().values_list('validation_code', flat=True)
    vd = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(6))
    while vd in vds:
        vd = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(6))
    EventMember.objects.create(user=person, event=event, validation_code=vd)
    send_mail_add_member_event(event_id, person, vd)
    return render(request, 'payment_done.html')


@csrf_exempt
def payment_canceled(request):
    return render(request, 'payment_failed.html')
