from django.contrib.auth.forms import UserCreationForm
from django.db import models
from django.contrib.auth.models import User, Group

# Create your models here.
from django import forms
from django.forms import Form
from django.utils import timezone


class Person(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, default=None)
    birth_date = models.DateField(default=timezone.now)
    is_intern = models.BooleanField(default=None)

    def __str__(self):
        return self.user.username


class Association(models.Model):
    name = models.CharField(max_length=128)
    chef = models.ForeignKey(Person, on_delete=models.CASCADE, default=None)

    def __str__(self):
        return self.name


class AssociationMember(models.Model):
    user = models.ForeignKey(Person, on_delete=models.CASCADE, default=None)
    association = models.ForeignKey(Association, on_delete=models.CASCADE, default=None)
    grade = models.CharField(max_length=64)

    def __str__(self):
        return self.association.name + ": " + self.user.__str__()


class Event(models.Model):
    title = models.CharField(max_length=64)
    description = models.TextField(default="Entrez une description ...")

    begin_date = models.DateTimeField(default=timezone.now)
    end_date = models.DateTimeField(default=timezone.now)
    end_inscription = models.DateTimeField(default=timezone.now)

    place = models.CharField(max_length=128, default="Epita")
    image = models.ImageField(upload_to='images', null=True, blank=True)

    association = models.ForeignKey(Association, on_delete=models.CASCADE)

    max_intern = models.PositiveIntegerField(default=0)
    max_extern = models.PositiveIntegerField(default=0)
    price_intern = models.PositiveIntegerField(default=0)
    price_extern = models.PositiveIntegerField(default=0)

    display_remaining_places = models.BooleanField(default=True)

    INFO_TYPE_CHOICES = [
        ('VALIDATED', 'Validé et ouvert'),
        ('CLOSED', 'Inscriptions terminés'),
        ('WAITING', 'En attente de validation'),
        ('TERMINATED', 'Evénement terminé'),
        ('REFUSED', 'Refusé'),
    ]

    info_type = models.CharField(max_length=10, choices=INFO_TYPE_CHOICES, default=None)
    creator = models.ForeignKey(Person, on_delete=models.CASCADE, default=None)

    def __str__(self):
        return self.title


class EventMember(models.Model):
    user = models.ForeignKey(Person, on_delete=models.CASCADE, default=None)
    event = models.ForeignKey(Event, on_delete=models.CASCADE, default=None)
    validation_code = models.CharField(max_length=6)

    def __str__(self):
        return self.event.__str__() + ": " + self.user.__str__()


class EventStaff(models.Model):
    user = models.ForeignKey(Person, on_delete=models.CASCADE, default=None)
    event = models.ForeignKey(Event, on_delete=models.CASCADE, default=None)

    def __str__(self):
        return self.event.__str__() + ": " + self.user.__str__()


class DateForm(Form):
    birth_date = forms.DateField()

    class Meta:
        fields = ('birth_date',)


class RegistrationForm(UserCreationForm):
    username = forms.CharField(label='Pseudo', max_length=30, required=True, help_text='Veuillez entrer un pseudo')
    first_name = forms.CharField(label='Prénom', max_length=30, required=True, help_text='Veuillez entrer votre prénom')
    last_name = forms.CharField(label='Nom', max_length=30, required=True, help_text='Veuillez entrer votre nom')
    email = forms.EmailField(max_length=254, required=True, help_text='Veuillez entrer votre addresse email,')
    password1 = forms.CharField(label='Mot de passe', min_length=8, max_length=64, widget=forms.PasswordInput(),
                                required=True, help_text='Entrer un mot de passe avec au moins 8 caractères. Le mot '
                                                         'de passe ne doit pas être similaire a votre pseudo.')
    password2 = forms.CharField(label='Confirmation de mot de passe', min_length=8, max_length=64,
                                widget=forms.PasswordInput(), required=True, help_text='Entrer le même mot de passe.')

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2',)
