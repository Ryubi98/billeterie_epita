# Projet Billeterie Epita - Yakachoisir


## Introduction

### Groupe

- Antonin Ginet - antonin.ginet

- Guillaume Saladin - guillaume.saladin

- Maxime Morales - maxime.morales

- Nicolas Acart - nicolas.acart

### Le Projet

Ce projet est la réponse à l'appel d'offres Billeterie du projet Yakachoisir.
Après plusieurs échanges et une rencontre avec notre client, nous avons implémenté la solution suivante.
Il s'agit d'une billeterie en ligne pour les associations de l'EPITA.


## Lancement

L'utilisation de PyCharm est recommandée pour ce projet.
La commande pour lancer le serveur est : __python manage.py runserver --insecure__
L'option __--insecure__ permet de charger les *statics files* lorsque le projet n'est pas en mode *DEBUG*.


## Architecture

Ce projet Django fonctionne grâce à un environnement virtuel Python.
Nous avons généré le fichier __requirements.txt__ qui liste les dépendances pour ce projet.
Pour les installer dans votre environnement virtuel, utilisez la commande suivante : __pip install -r requirements.txt__

Le projet Django se nomme __Yakastar__. Le fichier correspondant contient les différents fichier Python qu'utilise Django.

Le dossier __billeterie__ est l'application Django du site. Il contient l'intégralité du code source avec :

    - le dossier __static__ : il contient les fichiers statics du site (css, js, img)

    - le dossier __templates__ : il contient tous les fichiers HTML du site. Le template de base étant __base.html__

    - les fichiers liés à Django : admin.py, models.py, urls.py, utils.py, views.py

    - le dossier __tests__ : il contient les tests du projet

    - le dossier __documentations_ : il contient toute la documentation liée au projet


## Documentations

Dans le dossier __documentations__ se trouve toute la documentation liée au projet.
On y retrouve tout d'abord les documents présentés lors de la rencontre avec le client :

    - bdd.pdf : schéma de la base de donnée

    - cahier.pdf : cahier des charges de la solution proposée

    - slides.pdf : slides de la présentation

    - toolkit.xlsx : toolkit du projet

    - user_stories.pdf : les user_stories du projet avec leurs estimations en heures


## Tests

Pour lancer les tests, il faut installer les dépendances du __requirements.txt__, notamment :

    - pytest

    - pytest-cov

    - PyYaml

Il suffit ensuite de lancer la commande suivante : __pytest__